/*
 * Copyright (C) 2016 JetRadar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jetradar.desertplaceholder;

import ohos.agp.components.AttrSet;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.app.Context;

public class DesertPlaceholder extends StackLayout {

    private final String DP_MESSAGE = "dp_message";
    private final String DP_BUTTONTEXT = "dp_buttonText";
    public static boolean animationEnabled = true;

    private Text button;
    private Text message;

    public DesertPlaceholder(Context context) {
        super(context);
        init(context, null);
    }

    public DesertPlaceholder(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    public DesertPlaceholder(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrSet) {
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_placeholder, this, true);
        message = (Text) findComponentById(ResourceTable.Id_placeholder_message);
        button = (Text) findComponentById(ResourceTable.Id_placeholder_button);
        if (attrSet != null && attrSet.getAttr(DP_MESSAGE).isPresent()) {
            setMessage(attrSet.getAttr(DP_MESSAGE).get().getStringValue());
        }
        if (attrSet != null && attrSet.getAttr(DP_BUTTONTEXT).isPresent()) {
            setButtonText(attrSet.getAttr(DP_BUTTONTEXT).get().getStringValue());
        }
        setBackground(ElementUtil.getShapeElement6(ElementUtil.getColor(getContext(), ResourceTable.Color_background_desert)));
    }

    public void setOnButtonClickListener(ClickedListener clickListener) {
        button.setClickedListener(clickListener);
    }

    public void setMessage(String msg) {
        message.setText(msg);
    }

    public void setButtonText(String action) {
        if (isEmpty(action)) {
            button.setVisibility(HIDE);
        } else {
            button.setText(action);
            button.setVisibility(VISIBLE);
        }
    }

    private boolean isEmpty(String str) {
        if (str != null) {
            return str.isEmpty();
        }
        return true;
    }

}