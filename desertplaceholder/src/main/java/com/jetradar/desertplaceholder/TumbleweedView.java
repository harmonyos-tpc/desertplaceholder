/*
 * Copyright (C) 2016 JetRadar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jetradar.desertplaceholder;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Matrix;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.jetradar.desertplaceholder.DesertPlaceholder.animationEnabled;

public class TumbleweedView extends Component implements Component.DrawTask {

    private static final int INVALID_TIME = -1;
    private static final float START_POSITION_PERCENT_OF_SCREEN = 0.3f;
    private static final float SHADOW_SCALE_FACTOR = 0.7f;
    private static final float MAX_JUMP_HEIGHT_IN_METERS = 1f;
    private static final float DEFAULT_SPEED = 50f; // dp/sec
    private static final float MAX_SPEED = DEFAULT_SPEED * 1.2f;
    private static final float MIN_SPEED = DEFAULT_SPEED * 0.8f;
    private static final float SPEED_RANDOM_DELTA = 0.05f * DEFAULT_SPEED;
    private static final float ROTATION_SPEED = 360;
    private static final float g = 3.71f;

    private float density;
    private double timeStamp = INVALID_TIME;
    private float meterInDp;
    private float currentVerticalSpeed;
    private float currentSpeed = DEFAULT_SPEED;

    private PixelMap tumbleweed;
    private PixelMap shadow;
    private SecureRandom random;
    private float x;
    private float y;
    private float angle;
    private Paint paint;
    private Matrix matrix = new Matrix();
    private float bottomPosition;
    private float topPosition;

    public TumbleweedView(Context context) {
        super(context);
        init(context);
    }

    public TumbleweedView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context);
    }

    public TumbleweedView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        paint = new Paint();
        tumbleweed = BitmapFactory.decodeResource(context, ResourceTable.Media_tumbleweed);
        shadow = BitmapFactory.decodeResource(context, ResourceTable.Media_shadow_tumbleweed);
        density = context.getResourceManager().getDeviceCapability().screenDensity / 160;
        try {
            random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(TumbleweedView.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        meterInDp = tumbleweed.getImageInfo().size.height;
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        long time = System.currentTimeMillis();

        if (timeStamp != INVALID_TIME) {
            double delta = (time - timeStamp) / 1000d;
            updatePosition(delta);
            drawShadow(canvas);
            drawTumbleweed(canvas, delta);
        } else {
            bottomPosition = getHeight() - meterInDp;
            topPosition = MAX_JUMP_HEIGHT_IN_METERS * density;
            x = getWidth() * START_POSITION_PERCENT_OF_SCREEN;
            y = bottomPosition;
        }
        timeStamp = time;
        if (animationEnabled) {
            getContext().getUITaskDispatcher().asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    invalidate();
                }
            });
        }
    }

    private void drawShadow(Canvas canvas) {
        float scale = 1 - SHADOW_SCALE_FACTOR * ((bottomPosition - y) / (bottomPosition - topPosition));
        PixelMap toDraw;
        if (scale == 1) {
            toDraw = shadow;
        } else {
            int width = Math.round(shadow.getImageInfo().size.width * scale);
            int height = Math.round(shadow.getImageInfo().size.height * scale);
            if (width == 0 || height == 0) {
                return;
            }
            // 放大位图，和component一样大小
            Rect rect = new Rect(0, 0, shadow.getImageInfo().size.width, shadow.getImageInfo().size.height);
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size(width, height);
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            toDraw = PixelMap.create(shadow, rect, initializationOptions);
        }
        canvas.drawPixelMapHolder(new PixelMapHolder(toDraw), x, getHeight() - shadow.getImageInfo().size.height, paint);
    }

    private void drawTumbleweed(Canvas canvas, double delta) {
        updateAngle(delta);
        matrix.setTranslate(x, y);
        matrix.postRotate(angle, x + tumbleweed.getImageInfo().size.width / 2, y + tumbleweed.getImageInfo().size.height / 2);
        canvas.rotate(angle, x + tumbleweed.getImageInfo().size.width / 2, y + tumbleweed.getImageInfo().size.height / 2);
        canvas.drawPixelMapHolder(new PixelMapHolder(tumbleweed), x, y, paint);
    }

    private void updateAngle(double delta) {
        angle += delta * ROTATION_SPEED;
        angle %= 360;
    }

    private void updatePosition(double delta) {
        recalculateCurrentSpeed();
        x += density * currentSpeed * delta;
        if (x > getWidth() + 5 * tumbleweed.getImageInfo().size.width) {
            x = -tumbleweed.getImageInfo().size.width;
        }

        if (y != bottomPosition || currentVerticalSpeed > 0) {
            currentVerticalSpeed -= delta * g * meterInDp;
            y -= delta * currentVerticalSpeed;
            if (y > bottomPosition) {
                y = bottomPosition;
            }
        } else {
            calculateJumpSpeed();
        }
    }

    private void calculateJumpSpeed() {
        currentVerticalSpeed = getRandom(0.2f * MAX_JUMP_HEIGHT_IN_METERS, MAX_JUMP_HEIGHT_IN_METERS) * meterInDp * density;
    }

    private void recalculateCurrentSpeed() {
        currentSpeed += getRandom(-SPEED_RANDOM_DELTA, SPEED_RANDOM_DELTA);

        if (currentSpeed < MIN_SPEED) {
            currentSpeed = MIN_SPEED;
        }
        if (currentSpeed > MAX_SPEED) {
            currentSpeed = MAX_SPEED;
        }
    }

    private float getRandom(float min, float max) {
        float suiji = 0.5f;
        if (this.random != null) {
            suiji = this.random.nextFloat();
        }
        return min + suiji * (max - min);
    }

}