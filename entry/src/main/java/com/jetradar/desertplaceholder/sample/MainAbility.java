package com.jetradar.desertplaceholder.sample;

import com.jetradar.desertplaceholder.DesertPlaceholder;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;

/**
 * 效果演示页面
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        DesertPlaceholder desertPlaceholder = (DesertPlaceholder) findComponentById(ResourceTable.Id_placeholder);
        desertPlaceholder.setOnButtonClickListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new ToastDialog(MainAbility.this).setText("Button clicked").show();
            }
        });
    }
}
